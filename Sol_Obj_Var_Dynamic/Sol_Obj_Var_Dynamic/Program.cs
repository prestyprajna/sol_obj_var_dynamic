﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Obj_Var_Dynamic
{
    class Program
    {
        static void Main(string[] args)
        {
            //object

            Student studentObj = new Student()
            {
                FirstName = "shiro",
                LastName = "sharma"
            };

            object obj1 = studentObj;
            //Console.WriteLine(obj1.FirstName);  //cant access firstName

            //studentObj = obj1;  //casting required


            obj1 = ".net";//we can change type as required
            obj1 = 34;


            //var keyword
            Student studentObj1 = new Student()
            {
                FirstName = "sahitya",
                LastName = "shetty"
            };

            var obj2 = studentObj1;  //no casting required
            Console.WriteLine(obj2.FirstName);
            Console.WriteLine(obj2.LastName);

            Console.WriteLine(studentObj1.FirstName);
            Console.WriteLine(studentObj1.LastName);

            //obj2 = ".net"; //cant change type

            //dynamic keyword
            Student studentObj2 = new Student()
            {
                FirstName = "presty",
                LastName = "prajna"
            };
            dynamic obj3 = studentObj2;

            Console.WriteLine(obj3.FirstName);
            Console.WriteLine(obj3.LastName);

            Console.WriteLine(studentObj2.FirstName);
            Console.WriteLine(studentObj2.LastName);

            obj3 = "hello world";  //can change type
            Console.WriteLine(obj3);

            obj3 = 87;
            Console.WriteLine(obj3);


            //anonymous type (using var keyword)

            var studentObj4 = new
            {
                FirstName = "saptha",
                LastName = "karkera"
            };

            Console.WriteLine(studentObj4.FirstName);
            Console.WriteLine(studentObj4.LastName);

            //copy anonymous object into strong type i.e,student class
            Student studentObj7 = new Student()
            {
                FirstName = studentObj4.FirstName,
                LastName = studentObj4.LastName
            };

            Console.WriteLine(studentObj7.FirstName);
            Console.WriteLine(studentObj7.LastName);

            //anonymous type (using dynamic keyword)

            dynamic studentObj9 = new
            {
                FirstName = "diwik",
                LastName = "suvarna"
            };
            Console.WriteLine(studentObj9.FirstName);
            Console.WriteLine(studentObj9.LastName);

            Student studentObj123 = new Student()
            {
                FirstName = studentObj9.FirstName,
                LastName = studentObj9.LastName
            };
            Console.WriteLine(studentObj123.FirstName);
            Console.WriteLine(studentObj123.LastName);
        }
    }

  

    public class Student
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
